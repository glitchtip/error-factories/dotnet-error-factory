﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace myWebApp.Pages
{
    public class DivideZeroModel : PageModel
    {
        private int DivideByZero(int inputNumber) {
            return 0/inputNumber;
        }

        public void OnGet()
        {
	    DivideByZero(0);
        }
    }
}
