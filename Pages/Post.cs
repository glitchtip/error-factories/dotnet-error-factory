﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace myWebApp.Pages
{
    public class PostModel : PageModel
    {
        private readonly ILogger<PostModel> _logger;

        public PostModel(ILogger<PostModel> logger)
        {
            _logger = logger;
        }
        public void OnPost()
        {
        throw new ArgumentException(
            "Bad POST");
        }
    }
}
